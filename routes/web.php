<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
// use App\Http\Controllers\RoleToPermissionController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\StaffController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



 
 // Route::get('/auth/login', [MainController::class, 'login'])->name('auth.login');
 // Route::get('/auth/register', [MainController::class, 'register'])->name('auth.register');
 Route::post('/auth/save', [MainController::class, 'save'])->name('auth.save');
 Route::post('/auth/check', [MainController::class, 'check'])->name('auth.check');
 Route::get('/auth/logout', [MainController::class, 'logout'])->name('auth.logout');

// Route::get('/admin/dashboard', [MainController::class, 'dashboard']); 

Route::group(['middleware'=>['AuthCheck']],function(){
Route::get('/auth/login', [MainController::class, 'login'])->name('auth.login');
Route::get('/auth/register', [MainController::class, 'register'])->name('auth.register');


Route::get('/admin/dashboard', [MainController::class, 'dashboard'])->name('admin.dashboard'); 

});


 Route::get('subscription/create', [SubscriptionController::class, 'create'])->name('subscription.create'); 
 Route::get('subscription/index', [SubscriptionController::class, 'index'])->name('subscription.index');
 Route::post('subscription/store', [SubscriptionController::class, 'store'])->name('subscription.store');
 Route::get('subscription/{id}', [SubscriptionController::class, 'show'])->name('subscription.show');
 Route::delete('subscription/{id}', [SubscriptionController::class, 'destroy'])->name('subscription.destroy');
 Route::get('subscription/edit/{id}', [SubscriptionController::class, 'edit'])->name('subscription.edit');
 Route::post('subscription/edit', [SubscriptionController::class, 'update']);
    

  // Route::resource('subscription', SubscriptionController::class);

Route::get('account/create', [AccountController::class, 'create'])->name('account.create'); 
Route::get('account/index', [AccountController::class, 'index'])->name('account.index');
Route::post('account/save', [AccountController::class, 'save'])->name('account.save');
Route::get('account/{id}', [AccountController::class, 'show'])->name('account.show');
Route::delete('account/{id}', [AccountController::class, 'destroy'])->name('account.destroy');
Route::get('account/edit/{id}', [AccountController::class, 'edit'])->name('account.edit');
Route::post('account/edit', [AccountController::class, 'update']);


Route::get('role/create',[RoleController::class, 'create'])->name('role.create'); 
Route::get('role/index', [RoleController::class, 'index'])->name('role.index');
Route::post('role/save', [RoleController::class, 'save'])->name('role.save');
Route::get('role/{id}', [RoleController::class, 'show'])->name('role.show');
Route::delete('role/{id}', [RoleController::class, 'destroy'])->name('role.destroy');
Route::get('role/edit/{id}', [RoleController::class, 'edit'])->name('role.edit');
Route::post('role/edit', [RoleController::class, 'update']);

Route::get('permission/create',[PermissionController::class, 'create'])->name('permission.create'); 
Route::get('permission/index', [PermissionController::class, 'index'])->name('permission.index');
Route::post('permission/save', [PermissionController::class, 'save'])->name('permission.save');
Route::get('permission/{id}', [PermissionController::class, 'show'])->name('permission.show');
Route::delete('permission/{id}', [PermissionController::class, 'destroy'])->name('permission.destroy');
Route::get('permission/edit/{id}', [PermissionController::class, 'edit'])->name('permission.edit');
Route::post('permission/edit', [PermissionController::class, 'update']);


// Route::get('roletopermission/create',[RoleToPermissionController::class, 'create'])->name('roletopermission.create'); 
// Route::get('roletopermission/index', [RoleToPermissionController::class, 'index'])->name('roletopermission.index');
// Route::post('roletopermission/save', [RoleToPermissionController::class, 'save'])->name('roletopermission.save');
// Route::get('roletopermission/{id}', [RoleToPermissionController::class, 'show'])->name('roletopermission.show');
// Route::delete('roletopermission/{id}',[RoleToPermissionController::class, 'destroy'])->name('roletopermission.destroy');
// Route::get('roletopermission/edit/{id}', [RoleToPermissionController::class, 'edit'])->name('roletopermission.edit');
// Route::post('roletopermission/edit', [RoleToPermissionController::class, 'update']);



Route::get('product/create',[ProductController::class, 'create'])->name('product.create'); 
Route::get('product/index', [ProductController::class, 'index'])->name('product.index');
Route::post('product/save', [ProductController::class, 'save'])->name('product.save');
Route::get('product/{id}', [ProductController::class, 'show'])->name('product.show');
Route::delete('product/{id}', [ProductController::class, 'destroy'])->name('product.destroy');
Route::get('product/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
Route::post('product/edit', [ProductController::class, 'update']);


Route::get('team/create',[TeamController::class, 'create'])->name('team.create'); 
Route::get('team/index', [TeamController::class, 'index'])->name('team.index');
Route::post('team/save', [TeamController::class, 'save'])->name('team.save');
Route::get('team/{id}', [TeamController::class, 'show'])->name('team.show');
Route::delete('team/{id}', [TeamController::class, 'destroy'])->name('team.destroy');
Route::get('team/edit/{id}', [TeamController::class, 'edit'])->name('team.edit');
Route::post('team/edit', [TeamController::class, 'update']);



Route::get('staff/create',[StaffController::class, 'create'])->name('staff.create'); 
Route::get('staff/index', [StaffController::class, 'index'])->name('staff.index');
Route::post('staff/save', [StaffController::class, 'save'])->name('staff.save');
Route::get('staff/{id}', [StaffController::class, 'show'])->name('staff.show');
Route::delete('staff/{id}', [StaffController::class, 'destroy'])->name('staff.destroy');
Route::get('staff/edit/{id}', [StaffController::class, 'edit'])->name('staff.edit');
Route::post('staff/edit', [StaffController::class, 'update']);










    

