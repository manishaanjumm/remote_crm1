<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;
      protected $table = 'staff';

     protected $fillable = [
        'full_name', 'user_name','password','status','role_id','team_id','account_id'
    ];
}
