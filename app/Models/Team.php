<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

      protected $table = 'team';

     protected $fillable = [
        'name', 'trades_request','qc_request','add_deal_potential','random_did','status','account_id'
    ];
}
