<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;
     protected $table = 'accounts_p';

     protected $fillable = [
        'admins_id', 'name','subdomain','expires_at','subscription_id'
    ];
}
