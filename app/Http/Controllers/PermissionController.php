<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\RoleTOPermissionController;
use App\Models\RoleTOPermission;


class PermissionController extends Controller
{
    public function index()
    {       
         $permissions = Permission::all();  
        return view('permission.index', compact('permissions'));  
 

    }

      public function create()
    {
        return view('permission.create');
    }


      public function save(Request $request)
    {
       //return $request->input();
     

        $this->validate($request, [
            'permission_name' => 'required',
            'permission_description' => 'required',
             'allowed' => 'required',
              'role_id' => 'required',
           
        ]); 

             
            $permission = new Permission; 
            $permission->permission_name = $request->permission_name;
            $permission->permission_description = $request->permission_description;
            $permission->allowed = $request->allowed;
            $permission->role_id = $request->role_id;
            $permission->save();


           // $roletopermission = new RoleTOPermission;
           
           //   $roletopermission->role_id = $request->role_id;
           //   $roletopermission->permission_id = $request->permission_id;  

           //    $roletopermission->save(); 
       
  //echo "<pre>";print_r($role);die;
     
       return redirect()->route('permission.index')
                        ->with('success','Record created successfully');
      
     }

        public function show($id)
    {


       $id_d =  base64_decode($id);
      // echo $id_d;die;
        $show_data = Permission::find($id_d);
      //  echo "<pre>";print_r($show_data);die;
        return view('permission.show',compact('show_data'));
    }

     public function destroy($id)
    {
       
        Permission::find($id)->delete();
  
        return redirect()->route('permission.index')
                        ->with('success','Role deleted successfully');
    
    }


      public function edit($id)
    {
        //echo base64_decode($id);die;
      $id_d =  base64_decode($id);

        $permission_edit = Permission::find($id_d);
        return view('permission.edit',compact('permission_edit','id_d'));
         
    }

     public function update(Request $request)
    {
      // $request->input();

          $this->validate($request, [
           'permission_name' => 'required',
            'permission_description' => 'required',
             'allowed' => 'required',
            
        ]); 
       

          $permission_update = Permission::find($request->id);
         // echo "<pre>";print_r($permission_update);die;

          $permission_update->permission_name = $request->permission_name;
          $permission_update->permission_description = $request->permission_description;
           $permission_update->allowed =  $request->allowed;

            $permission_update->save();
       
        return redirect()->route('permission.index')
                        ->with('success','permission updated successfully');
    }   
}
