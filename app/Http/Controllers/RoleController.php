<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Admin;
use App\Http\Controllers\MainController;

class RoleController extends Controller
{
   public function index()
    {       
         $roles = Role::all();  
        return view('role.index', compact('roles'));  
 

    }

      public function create()
    {
        return view('role.create');
    }


      public function save(Request $request)
    {
       //return $request->input();
     

        $this->validate($request, [
            'role_name' => 'required',
            'role_description' => 'required',
           
        ]); 

                $data = Admin::where('email','=',$request->admin_id)->first();
                   // echo "<pre>";print_r($data);die;
                $id = $data['id'];
 //echo $id;die;
            $role = new Role; 
            $role->role_name = $request->role_name;
            $role->role_description = $request->role_description;
            $role->admin_id = $id;
            $role->save();
       
  //echo "<pre>";print_r($role);die;
     
       return redirect()->route('role.index')
                        ->with('success','Record created successfully');
      
     }

        public function show($id)
    {

//echo $role_id;die;
       $id_d =  base64_decode($id);
      // echo $id_d;die;
        $show_data = Role::find($id_d);
      //  echo "<pre>";print_r($show_data);die;
        return view('role.show',compact('show_data'));
    }

     public function destroy($id)
    {
       
        Role::find($id)->delete();
  
        return redirect()->route('role.index')
                        ->with('success','Role deleted successfully');
    
    }


      public function edit($id)
    {
        //echo base64_decode($id);die;
      $id_d =  base64_decode($id);

        $role_edit = Role::find($id_d);
        return view('role.edit',compact('role_edit','id_d'));
         
    }

     public function update(Request $request)
    {
      // $request->input();

          $this->validate($request, [
            'role_name' => 'required',
            'role_description' => 'required',
            
        ]); 
       // return $request->input();
         $data = Admin::where('email','=',$request->admin_id)->first();
                   // echo "<pre>";print_r($data);die;
                $id = $data['id'];

          $role_update = Role::find($request->id);
         // echo "<pre>";print_r($role_update);die;

          $role_update->role_name = $request->role_name;
           $role_update->role_description = $request->role_description;
              $role_update->admin_id = $id;

            $role_update->save();
       
        return redirect()->route('role.index')
                        ->with('success','Role updated successfully');
    }  
}
