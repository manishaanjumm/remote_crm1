<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\RoleToPermission;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class RoleToPermissionController extends Controller
{
     public function index()
    {       
         $roletopermissions = RoleToPermission::all();  
        return view('roletopermission.index', compact('roletopermissions'));  
 

    }

      public function create()
    {
        return view('roletopermission.create');
    }


      public function save(Request $request)
    {
       //return $request->input();
     

        $this->validate($request, [
            'role_id' => 'required',
            'permission_id' => 'required',
           
        ]); 

         

            $roletopermission = new RoleToPermission; 
            $roletopermission->role_id = $request->role_id;
            $roletopermission->permission_id = $request->permission_id;
           
            $roletopermission->save();
   
     
       return redirect()->route('roletopermission.index')
                        ->with('success','Record created successfully');
      
     }



       public function show($id)
    {
       $id_d =  base64_decode($id);
        $show_data = RoleToPermission::find($id_d);
        return view('roletopermission.show',compact('show_data'));
    }

     public function destroy($id)
    {
       
        RoleToPermission::find($id)->delete();
  
        return redirect()->route('roletopermission.index')
                        ->with('success','RoleToPermission deleted successfully');
    
    }


      public function edit($id)
    {
        //echo base64_decode($id);die;
      $id_d =  base64_decode($id);

        $roletopermission_edit = RoleToPermission::find($id_d);
        return view('roletopermission.edit',compact('roletopermission_edit','id_d'));
         
    }

     public function update(Request $request)
    {
      
          $this->validate($request, [
            'role_id' => 'required',
            'permission_id' => 'required',
        ]); 
        //return $request->input();
       

          $roletopermission_update = RoleToPermission::find($request->id);
       

           $roletopermission_update->role_id = $request->role_id;
            $roletopermission_update->permission_id = $request->permission_id;
           
           

            $roletopermission_update->save();
       
        return redirect()->route('roletopermission.index')
                        ->with('success','RoleToPermission updated successfully');
    }
}
