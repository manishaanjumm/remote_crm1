<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Staff;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class StaffController extends Controller
{
   
     public function index()
    {       
         $staffs = Staff::all();  
        return view('staff.index', compact('staffs'));  
 

    }

      public function create()
    {
        return view('staff.create');
    }


      public function save(Request $request)
    {
       // return $request->input();

        $this->validate($request, [
            'full_name' => 'required',
            'user_name' => 'required|unique:staff,user_name',
            'password' => 'required|min:5|max:12',
             'status' => 'required',
             'role_id' => 'required',
             'account_id' => 'required',
             'team_id' => 'required',
            
        ]); 



              
             $staff = new Staff; 
            $staff->full_name = $request->full_name;
            $staff->user_name = $request->user_name;
             $staff->password = $request->password;
            $staff->status = $request->status;
            $staff->role_id = $request->role_id;
            $staff->account_id = $request->account_id;
            $staff->team_id = $request->team_id;

            
          
            $staff->save();
       
  
     
       return redirect()->route('staff.index')
                        ->with('success','Record created successfully');
      
     }

        public function show($id)
    {
       $id_d =  base64_decode($id);
        $show_data = Staff::find($id_d);
        return view('staff.show',compact('show_data'));
    }

     public function destroy($id)
    {
       
        Staff::find($id)->delete();
  
        return redirect()->route('staff.index')
                        ->with('success','Staff deleted successfully');
    
    }


      public function edit($id)
    {
        //echo base64_decode($id);die;
      $id_d =  base64_decode($id);

        $staff_edit = Staff::find($id_d);
        return view('staff.edit',compact('staff_edit','id_d'));
         
    }

     public function update(Request $request)
    {
      

          $this->validate($request, [
          'full_name' => 'required',
            'user_name' => 'required',
            'password' => 'required|min:5|max:12',
             'role_id' => 'required',
             'account_id' => 'required',
             'team_id' => 'required',
        ]); 

        //  return $request->input();
       

          $staff_update = Staff::find($request->id);
       

        
   
            $staff_update->full_name = $request->full_name;
            $staff_update->user_name = $request->user_name;
            $staff_update->password = $request->password;
            $staff_update->status = $request->status;
            $staff_update->role_id = $request->role_id;
            $staff_update->account_id = $request->account_id;
            $staff_update->team_id = $request->team_id;



            $staff_update->save();
       
        return redirect()->route('staff.index')
                        ->with('success','Staff updated successfully');
    }


}
