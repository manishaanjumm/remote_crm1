<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Account;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Admin;
 use App\Http\Controllers\MainController;

class AccountController extends Controller
{

  public function index()
    {       
         $accounts = Account::all();  
        return view('account.index', compact('accounts'));  
 

    }

      public function create()
    {
        return view('account.create');
    }


      public function save(Request $request)
    {
       //return $request->input();
      // "name":"Xiaomi","subdomain":"qwqw","expires_at":"07\/01\/2021","subscription":"5","admin_1":"1"

        $this->validate($request, [
            'name' => 'required',
            'subdomain' => 'required',
            'subscription_id' => 'required',
             'expires_at' => 'required',
        ]); 

                $data = Admin::where('email','=',$request->admins_id)->first();
                   // echo "<pre>";print_r($data);die;
                $id = $data['id'];
 //echo $id;die;
            $account = new Account; 
            $account->name = $request->name;
            $account->subdomain = $request->subdomain;
            $account->expires_at = $request->expires_at;
            $account->subscription_id = $request->subscription_id;
            $account->admins_id = $id;
            $account->save();
       
   //  $account_insert=  Account::create($request->all());
     // die;

     // if($account_insert){
     //       return back()->with('success','Data have been successfully inserted');
     //    }else{
     //         return back()->with('fail','Something went wrong');
     //    }
     
       return redirect()->route('account.index')
                        ->with('success','Record created successfully');
      
     }

        public function show($id)
    {
       $id_d =  base64_decode($id);
        $show_data = Account::find($id_d);
        return view('account.show',compact('show_data'));
    }

     public function destroy($id)
    {
       
        Account::find($id)->delete();
  
        return redirect()->route('account.index')
                        ->with('success','Account deleted successfully');
    
    }


      public function edit($id)
    {
        //echo base64_decode($id);die;
      $id_d =  base64_decode($id);

        $account_edit = Account::find($id_d);
        return view('account.edit',compact('account_edit','id_d'));
         
    }

     public function update(Request $request)
    {
       // $request->input();

          $this->validate($request, [
            'name' => 'required',
            'subdomain' => 'required',
            'subscription_id' => 'required',
             'expires_at' => 'required',
        ]); 
        //return $request->input();
         $data = Admin::where('email','=',$request->admins_id)->first();
                   // echo "<pre>";print_r($data);die;
                $id = $data['id'];

          $account_update = Account::find($request->id);
        //  echo "<pre>";print_r($account_update);die;

          $account_update->name = $request->name;
           $account_update->subdomain = $request->subdomain;
            $account_update->expires_at = $request->expires_at;
             $account_update->subscription_id = $request->subscription_id;
              $account_update->admins_id = $id;

            $account_update->save();
       
        return redirect()->route('account.index')
                        ->with('success','Account updated successfully');
    }
}

