<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Team;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{
     public function index()
    {       
         $teams = Team::all();  
        return view('team.index', compact('teams'));  
 

    }

      public function create()
    {
        return view('team.create');
    }


      public function save(Request $request)
    {
       // return $request->input();

        $this->validate($request, [
            'name' => 'required',
            'trades_request' => 'required',
            'qc_request' => 'required',
             'add_deal_potential' => 'required',
             'random_did' => 'required',
             'account_id' => 'required',
            
        ]); 



              
             $team = new Team; 
            $team->name = $request->name;
            $team->trades_request = $request->trades_request;
            $team->qc_request = $request->qc_request;
            $team->add_deal_potential = $request->add_deal_potential;
            $team->random_did = $request->random_did;
            $team->account_id = $request->account_id;
            $team->status = $request->status;

            
          
            $team->save();
       
  
     
       return redirect()->route('team.index')
                        ->with('success','Record created successfully');
      
     }

        public function show($id)
    {
       $id_d =  base64_decode($id);
        $show_data = Team::find($id_d);
        return view('team.show',compact('show_data'));
    }

     public function destroy($id)
    {
       
        Team::find($id)->delete();
  
        return redirect()->route('team.index')
                        ->with('success','Team deleted successfully');
    
    }


      public function edit($id)
    {
        //echo base64_decode($id);die;
      $id_d =  base64_decode($id);

        $team_edit = Team::find($id_d);
        return view('team.edit',compact('team_edit','id_d'));
         
    }

     public function update(Request $request)
    {
      

          $this->validate($request, [
           'name' => 'required',
            'trades_request' => 'required',
            'qc_request' => 'required',
             'add_deal_potential' => 'required',
             'random_did' => 'required',
             'account_id' => 'required',
        ]); 
       

          $team_update = Team::find($request->id);
       

        
   
            $team_update->name = $request->name;
            $team_update->trades_request = $request->trades_request;
            $team_update->qc_request = $request->qc_request;
            $team_update->add_deal_potential = $request->add_deal_potential;
            $team_update->random_did = $request->random_did;
            $team_update->account_id = $request->account_id;
            $team_update->status = $request->status;



            $team_update->save();
       
        return redirect()->route('team.index')
                        ->with('success','Team updated successfully');
    }
}
