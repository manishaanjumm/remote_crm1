<?php

namespace App\Http\Controllers;

 use App\Models\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\DB;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
         $subscriptions = Subscription::all();  
        return view('subscription.index', compact('subscriptions'));  
 

    }

     public function create()
    {
        return view('subscription.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//     public function add(Request $request)
//     {

//         // return $request->input();
//         $request->validate([
//             'type' => 'required',
//             'amount' => 'required',
//             'duration' => 'required',
//         ]);

//      $data = array();
// $data =new \DateTime();

//         $subcription_insert = DB::table('subscriptions')->insert([
//             'type'=>$request->input('type'),
//              'amount'=>$request->input('amount'),
//               'duration'=>$request->input('duration'),
//               'created_at'=>$data,
//               'updated_at'=>$data
//         ]);

//         if($subcription_insert){
//            return back()->with('success','Data have been successfully inserted');
//         }else{
//              return back()->with('fail','Something went wrong');
//         }
        
//     }


    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required',
            'amount' => 'required|numeric',
            'duration' => 'required|numeric',
        ]); 
       
     $subcription_insert=  Subscription::create($request->all());
     
       
        // if($subcription_insert){
        //    return back()->with('success','Data have been successfully inserted');
        // }else{
        //      return back()->with('fail','Something went wrong');
        // }
         return redirect()->route('subscription.index')
                        ->with('success','Record created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
   

     public function show($id)
    {
        $show_data = Subscription::find($id);
        return view('subscription.show',compact('show_data'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //echo $id;die;

        $subscription_edit = Subscription::find($id);
        return view('subscription.edit',compact('subscription_edit','id'));
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //return $request->input();

          $subscription_update = Subscription::find($request->id);

          $subscription_update->type = $request->type;
           $subscription_update->amount = $request->amount;
            $subscription_update->duration = $request->duration;

            $subscription_update->save();
       
        return redirect()->route('subscription.index')
                        ->with('success','Subscription updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        Subscription::find($id)->delete();
  
        return redirect()->route('subscription.index')
                        ->with('success','Subscription deleted successfully');
    
    }
}
