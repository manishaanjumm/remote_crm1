<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
    public function index()
    {       
         $products = Product::all();  
        return view('product.index', compact('products'));  
 

    }

      public function create()
    {
        return view('product.create');
    }


      public function save(Request $request)
    {
       // return $request->input();

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'currency' => 'required',
             'value' => 'required',
             'role_id' => 'required',
             'account_id' => 'required',
            
        ]); 



              
             $product = new Product; 
            $product->name = $request->name;
            $product->description = $request->description;
            $product->currency = $request->currency;
            $product->value = $request->value;
            $product->role_id = $request->role_id;
            $product->account_id = $request->account_id;
            $product->status = $request->status;

            
          
            $product->save();
       
  
     
       return redirect()->route('product.index')
                        ->with('success','Record created successfully');
      
     }

        public function show($id)
    {
       $id_d =  base64_decode($id);
        $show_data = Product::find($id_d);
        return view('product.show',compact('show_data'));
    }

     public function destroy($id)
    {
       
        Product::find($id)->delete();
  
        return redirect()->route('product.index')
                        ->with('success','Product deleted successfully');
    
    }


      public function edit($id)
    {
        //echo base64_decode($id);die;
      $id_d =  base64_decode($id);

        $product_edit = Product::find($id_d);
        return view('product.edit',compact('product_edit','id_d'));
         
    }

     public function update(Request $request)
    {
      

          $this->validate($request, [
           'name' => 'required',
            'description' => 'required',
            'currency' => 'required',
             'value' => 'required',
             'role_id' => 'required',
             'account_id' => 'required',
        ]); 
       

          $product_update = Product::find($request->id);
       

        
   
            $product_update->name = $request->name;
            $product_update->description = $request->description;
            $product_update->currency = $request->currency;
            $product_update->value = $request->value;
            $product_update->role_id = $request->role_id;
            $product_update->account_id = $request->account_id;
            $product_update->status = $request->status;



            $product_update->save();
       
        return redirect()->route('product.index')
                        ->with('success','Product updated successfully');
    }
}
