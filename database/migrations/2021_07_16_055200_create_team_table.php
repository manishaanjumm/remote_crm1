<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team', function (Blueprint $table) {
            $table->id();
           // $table->uuid('id')->primary();
            $table->string('name');
            $table->integer('trades_request');
            $table->integer('qc_request');
            $table->string('add_deal_potential');
            $table->string('random_did');
              $table->boolean('status');
            $table->uuid('account_id');
           // $table->dateTime('created_at');
           // $table->dateTime('updated_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team');
    }
}
