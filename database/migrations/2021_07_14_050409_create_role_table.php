<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role', function (Blueprint $table) {
             $table->id();
            // $table->timestamps();
           // $table->uuid('role_id')->primary();
            $table->string('role_name');
            $table->string('role_description');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->uuid('admin_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role');
    }
}
