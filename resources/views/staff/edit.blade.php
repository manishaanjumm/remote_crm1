<?php 
 use App\Http\Controllers\StaffController;
use App\Models\Staff;
use App\Http\Controllers\AccountController;
use App\Models\Account;
use App\Http\Controllers\TeamController;
use App\Models\Team;
use App\Http\Controllers\RoleController;
use App\Models\Role;

use Illuminate\Http\Request;


$data4 = Role::all();
$data3 = Team::all();
$data2 = Account::all();

 $data1 = Staff::all();
?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Staff</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Staff</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
         
              
              <!-- Add New User-->
         <div class="card popcard" id="adduser-sec">
        <div class="card-body">
       

       <div class="pull-right">
                <a class="btn btn-info" href="{{ route('staff.index') }}"> Back</a>
            </div>

          @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
          <form action="/staff/edit" method="POST">
            @csrf  
          <input type="hidden" name="id" value="{{$staff_edit->id}}"> 
  
    
            <div class="row">
                  <div class="col-12 col-md-6">
                       <div class="row">

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Full_name</label> <input type="text" value="{{ $staff_edit->full_name }}" name="full_name" placeholder="Enter Full_name" class="form-control"> 
                           <span class="text-danger">@error('full_name'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">User_name</label>
                           <input type="text" class="form-control" name="user_name" value="{{ $staff_edit->user_name }}" placeholder="Enter user_name">
                           <span class="text-danger">@error('user_name'){{ $message }} @enderror</span>
                         </div>
                        </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Old Password</label>
<input type="text" value="{{ $staff_edit->password }}" name="old_password"  class="form-control"> 
                           
                          
                         </div>
                        </div>

                         <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">New Password</label>
<input type="password" value="{{ old('password') }}" name="password" placeholder="Enter Password" class="form-control"> 
                          
                           <span class="text-danger">@error('password'){{ $message }} @enderror</span>
                         </div>
                        </div>

                         <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Role</label> 
                       <select name="role_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data4 as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->role_name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('role_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Account</label> 
                       <select name="account_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data2 as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('account_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Team</label> 
                       <select name="team_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data3 as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('team_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Status</label>
                          <input type="hidden" name="status" value="0">
                              <input type="checkbox" name="status" value="1">
                         </div>
                        </div>


                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Edit</button>
           </form>
        </div>
        </div>
              <!-- End Of New User-->
              
           
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

@endsection

	
							
						
