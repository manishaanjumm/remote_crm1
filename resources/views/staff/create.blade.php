<?php 
 use App\Http\Controllers\StaffController;
use App\Models\Staff;
use App\Http\Controllers\AccountController;
use App\Models\Account;
use App\Http\Controllers\TeamController;
use App\Models\Team;
use App\Http\Controllers\RoleController;
use App\Models\Role;

use Illuminate\Http\Request;

$data4 = Role::all();
$data3 = Team::all();
$data2 = Account::all();

 $data1 = Staff::all();


?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Staff</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Staff</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
          
              
              <!-- Add New User-->
              <div class="card popcard" id="adduser-sec">
                 <div class="card-body">
               

                     <div class="pull-right">
                <a class="btn btn-info" href="{{ route('staff.index') }}"> Back</a>
            </div>
                  <h4 class="card-title">Add New Staff</h4>

                     @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif

                  <form action="{{ route('staff.save') }}" method="POST">
    @csrf
                   
                   <div class="row">
                  <div class="col-12 col-md-6">
                      <div class="row">

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Full_name</label> <input type="text" value="{{ old('full_name') }}" name="full_name" placeholder="Enter Full_name" class="form-control"> 
                           <span class="text-danger">@error('full_name'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">User_name</label>
                           <input type="text" class="form-control" name="user_name" value="{{ old('user_name') }}" placeholder="Enter user_name">
                           <span class="text-danger">@error('user_name'){{ $message }} @enderror</span>
                         </div>
                        </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Password</label> <input type="password" value="{{ old('password') }}" name="password" placeholder="Enter Password" class="form-control"> 
                           <span class="text-danger">@error('password'){{ $message }} @enderror</span>
                         </div>
                        </div>

                         <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Role</label> 
                       <select name="role_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data4 as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->role_name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('role_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Account</label> 
                       <select name="account_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data2 as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('account_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Team</label> 
                       <select name="team_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data3 as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('team_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Status</label>
                          <input type="hidden" name="status" value="0">
                              <input type="checkbox" name="status" value="1">
                         </div>
                        </div>

                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>
                  
                  </form>
                 </div>
              </div>
              <!-- End Of New User-->
              
           
             <!-- Table Sec-->
              <div class="card">
                 <div class="card-body">
                
                  <h4 class="card-title">Staff List</h4>
                  <h6 class="card-subtitle">Total 10 result found.</h6><br/><br/>
                  <div class="table-responsive">
                   <table class="table dataTable">
                    <thead>
                       <tr>
                        <th>Full_name</th>
                        <th>User_name</th>
                         <th>Role</th>
                          <th>Account</th>
                           <th>Team</th>
                        
                        <th class="table-option">Action</th>
                       </tr>
                    </thead>
                    <?php foreach($data1 as $key => $staff) {
                   $check = Role::where('id','=',$staff->role_id)->first();
                    // echo "<pre>";print_r($check);die;
                       $role_name = $check['role_name'];
                        $check1 = Account::where('id','=',$staff->account_id)->first();
                    // echo "<pre>";print_r($check);die;
                       $account_name = $check1['name'];
                        $check2 = Team::where('id','=',$staff->team_id)->first();
                    // echo "<pre>";print_r($check);die;
                       $team_name = $check2['name'];
                    
                ?>
                       <tr>
                       <td>{{$staff->full_name}}</td>
                        <td>{{$staff->user_name}}</td>
                         <td>{{$role_name}}</td>
                           <td>{{$account_name}}</td>
                             <td>{{$team_name}}</td>
                       
                      
                        <td class="table-option">
                         <div class="btn-group">
                        <form action="{{ route('staff.destroy',$staff->id) }}" method="POST">
               
                          <a href="{{ route('staff.show',base64_encode($staff->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View Staff" ><i class="fa fa-arrow-circle-right"></i></a> 

   <a href="{{ route('staff.edit',base64_encode($staff->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="Edit Staff" ><i class="fa fa-edit"></i></a> 

                          <button  type="submit" class="btn btn-danger btn-sm deleteUser" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="Delete Staff"><i class="fa fa-trash"></i></button>
                        </div>

   @csrf  
                  @method('DELETE')  
                  
</form>
                        </td>
                       </tr>
                        <?php } ?>
                  
                   </table>
                   
                  </div>
                  
                 </div>
              </div>
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->
       <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).on('click','.deleteUser',function() {
        var url = $(this).attr('rel');
        if(confirm("Are you sure you want to delete this?")){
           window.location.href = url
        }
        else{
            return false;
        }
    })
</script>

@endsection

  
              
     <?php //{{ route('account.show',base64_encode($account->id)) }}
    // {{ route('account.destroy',$account->id) }}
   ?>         
