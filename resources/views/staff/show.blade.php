<?php //echo "<pre>";print_r($accounts);die;
use App\Http\Controllers\AccountController;
use App\Models\Account;
use App\Http\Controllers\TeamController;
use App\Models\Team;
use App\Http\Controllers\RoleController;
use App\Models\Role;
?>

@extends('layouts.template')
@section('content')
	<div class="page-wrapper">
		<div class="container-fluid">
			<div>
					  <div class="row page-titles">
						 <div class="col-md-6 col-8 align-self-center">
							<h3 class="text-themecolor m-b-0 m-t-0">Staff</h3>
							<ol class="breadcrumb">
							   <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
							   <li class="breadcrumb-item active">Staff</li>
							</ol>
						 </div>
					  </div>

					  	<div class="row">
        <div class="col-lg-12 margin-tb">
            
            <div class="pull-right" style="margin-bottom: 4px">
                <a class="btn btn-info" href="{{ route('staff.index') }}"> Back</a>
            </div>
        </div>
    </div>	
   
                   <?php 
                   $check = Role::where('id','=',$show_data->role_id)->first();
                    // echo "<pre>";print_r($check);die;
                       $role_name = $check['role_name'];
                        $check1 = Account::where('id','=',$show_data->account_id)->first();
                    // echo "<pre>";print_r($check);die;
                       $account_name = $check1['name'];
                        $check2 = Team::where('id','=',$show_data->team_id)->first();
                    // echo "<pre>";print_r($check);die;
                       $team_name = $check2['name'];
                    
                ?>
					  <div class="row">
						 <div class="col-12">
				 	
							<div class="card">
								<div class="card-body" style="font-size: 18px;">
									<p><small class="text-muted">Full Name</small> <br/> <b> {{ $show_data->full_name }}</b></p>
									<p><small class="text-muted">Username</small> <br/> <b> {{ $show_data->user_name }}</b> </p>
									<p><small class="text-muted">Role</small> <br/> <b> {{ $role_name }}</b> </p>
									<p><small class="text-muted">Account</small> <br/> <b> {{ $account_name }}</b> </p>
									<p><small class="text-muted">Team</small> <br/> <b> {{ $team_name }}</b> </p>
									
									
								</div>
							</div>
						 </div>
					  </div>
			</div>
		</div>
	</div>
@endsection