<?php //echo "<pre>";print_r($accounts);die;?>


@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Team</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Team</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">

              

  @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
                 <!-- Table Sec-->
              <div class="card">
            <div class="card">
                 <div class="card-body">
                  <div class="pull-right">
                <a class="btn btn-info" href="{{ route('team.create') }}"> Create Team</a>
            </div>
                  <h4 class="card-title">Team List</h4>
                  <h6 class="card-subtitle">Total 10 result found.</h6><br/><br/>
                  <div class="table-responsive">
                   <table class="table dataTable">
                    <thead>
                       <tr>
                        <th>Name</th>
                        <th>Trades_request</th>
                        <th>Qc_request</th>
                         <th>Add_deal_potential</th>
                          <th>Random_did</th>
                        <th class="table-option">Action</th>
                       </tr>
                    </thead>
                     @foreach ($teams as $key => $team)
                  
                       <tr>
                        <td>{{$team->name}}</td>
                        <td>{{$team->trades_request}}</td>
                         <td>{{$team->qc_request}}</td>
                          <td>{{$team->add_deal_potential}}</td>
                            <td>{{$team->random_did}}</td>
                      
                         
                        <td class="table-option">
                         <div class="btn-group">
                        <form action="{{ route('team.destroy',$team->id) }}" method="POST">
            
                           <a href="{{ route('team.show',base64_encode($team->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View Team" ><i class="fa fa-arrow-circle-right"></i></a> 

   <a href="{{ route('team.edit',base64_encode($team->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="Edit Team" ><i class="fa fa-edit"></i></a> 


                          <button  type="submit" class="btn btn-danger btn-sm deleteUser" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="Delete Team"><i class="fa fa-trash"></i></button>
                        </div>

   @csrf  
                  @method('DELETE')  
                  
</form>
                        </td>
                       </tr>
                        @endforeach
                  
                   </table>
                   
                  </div>
                  
                 </div>
              </div>
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).on('click','.deleteUser',function() {
        var url = $(this).attr('rel');
        if(confirm("Are you sure you want to delete this?")){
           window.location.href = url
        }
        else{
            return false;
        }
    })
</script>

@endsection


  