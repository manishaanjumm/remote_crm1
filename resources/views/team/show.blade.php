@extends('layouts.template')
@section('content')
	<div class="page-wrapper">
		<div class="container-fluid">
			<div>
					  <div class="row page-titles">
						 <div class="col-md-6 col-8 align-self-center">
							<h3 class="text-themecolor m-b-0 m-t-0">Team</h3>
							<ol class="breadcrumb">
							   <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
							   <li class="breadcrumb-item active">Team</li>
							</ol>
						 </div>
					  </div>

					  	<div class="row">
        <div class="col-lg-12 margin-tb">
            
            <div class="pull-right" style="margin-bottom: 4px">
                <a class="btn btn-info" href="{{ route('team.index') }}"> Back</a>
            </div>
        </div>
    </div>	

					  <div class="row">
						 <div class="col-12">
				 	
							<div class="card">
								<div class="card-body" style="font-size: 18px;">
									<p><small class="text-muted">Name</small> <br/> <b> {{ $show_data->name }}</b></p>
									<p><small class="text-muted">Trades_request</small> <br/> <b> {{ $show_data->trades_request }}</b> </p>
									<p><small class="text-muted">Qc_request</small> <br/> <b> {{ $show_data->qc_request }}</b> </p>
									<p><small class="text-muted">Add_deal_potential</small> <br/> <b> {{ $show_data->add_deal_potential }}</b> </p>
									<p><small class="text-muted">Random_did</small> <br/> <b> {{ $show_data->random_did }}</b> </p>
									
								</div>
							</div>
						 </div>
					  </div>
			</div>
		</div>
	</div>
@endsection