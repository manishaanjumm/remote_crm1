<?php 
 use App\Http\Controllers\TeamController;
use App\Models\Team;
use App\Http\Controllers\AccountController;
use App\Models\Account;

use Illuminate\Http\Request;



$data2 = Account::all();

 $data1 = Team::all();


?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Team</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Team</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
          
              
              <!-- Add New User-->
              <div class="card popcard" id="adduser-sec">
                 <div class="card-body">
               

                     <div class="pull-right">
                <a class="btn btn-info" href="{{ route('team.index') }}"> Back</a>
            </div>
                  <h4 class="card-title">Add New Team</h4>

                     @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif

                  <form action="{{ route('team.save') }}" method="POST">
    @csrf
                   
                   <div class="row">
                  <div class="col-12 col-md-6">
                      <div class="row">

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Name</label> <input type="text" value="{{ old('name') }}" name="name" placeholder="Enter Name" class="form-control"> 
                           <span class="text-danger">@error('name'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Trades_request</label>
                           <input type="text" class="form-control" name="trades_request" value="{{ old('trades_request') }}" placeholder="Enter Trades_request">
                           <span class="text-danger">@error('trades_request'){{ $message }} @enderror</span>
                         </div>
                        </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Qc_request</label> <input type="text" value="{{ old('qc_request') }}" name="qc_request" placeholder="Enter Qc_request" class="form-control"> 
                           <span class="text-danger">@error('qc_request'){{ $message }} @enderror</span>
                         </div>
                        </div>

                         <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Add_deal_potential</label> <input type="text" value="{{ old('add_deal_potential') }}" name="add_deal_potential" placeholder="Enter Add_deal_potential" class="form-control"> 
                           <span class="text-danger">@error('add_deal_potential'){{ $message }} @enderror</span>
                         </div>
                        </div>
                         <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Random_did</label> <input type="text" value="{{ old('random_did') }}" name="random_did" placeholder="Enter Random_did" class="form-control"> 
                           <span class="text-danger">@error('random_did'){{ $message }} @enderror</span>
                         </div>
                        </div> 

                   

                    <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Account</label> 
                       <select name="account_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data2 as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('account_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Status</label>
                          <input type="hidden" name="status" value="0">
                              <input type="checkbox" name="status" value="1">
                         </div>
                        </div>

                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>
                  
                  </form>
                 </div>
              </div>
              <!-- End Of New User-->
              
           
             <!-- Table Sec-->
              <div class="card">
                 <div class="card-body">
                
                  <h4 class="card-title">Team List</h4>
                  <h6 class="card-subtitle">Total 10 result found.</h6><br/><br/>
                  <div class="table-responsive">
                   <table class="table dataTable">
                    <thead>
                       <tr>
                        <th>Name</th>
                        <th>Trades_request</th>
                        <th>Qc_request</th>
                         <th>Add_deal_potential</th>
                          <th>Random_did</th>
                        <th class="table-option">Action</th>
                       </tr>
                    </thead>
                     @foreach ($data1 as $key => $team)
                  
                       <tr>
                       <td>{{$team->name}}</td>
                        <td>{{$team->trades_request}}</td>
                         <td>{{$team->qc_request}}</td>
                          <td>{{$team->add_deal_potential}}</td>
                            <td>{{$team->random_did}}</td>
                      
                        <td class="table-option">
                         <div class="btn-group">
                        <form action="{{ route('team.destroy',$team->id) }}" method="POST">
               
                          <a href="{{ route('team.show',base64_encode($team->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View Team" ><i class="fa fa-arrow-circle-right"></i></a> 

   <a href="{{ route('team.edit',base64_encode($team->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="Edit Team" ><i class="fa fa-edit"></i></a> 

                          <button  type="submit" class="btn btn-danger btn-sm deleteUser" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="Delete Team"><i class="fa fa-trash"></i></button>
                        </div>

   @csrf  
                  @method('DELETE')  
                  
</form>
                        </td>
                       </tr>
                        @endforeach
                  
                   </table>
                   
                  </div>
                  
                 </div>
              </div>
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->
       <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).on('click','.deleteUser',function() {
        var url = $(this).attr('rel');
        if(confirm("Are you sure you want to delete this?")){
           window.location.href = url
        }
        else{
            return false;
        }
    })
</script>

@endsection

  
              
     <?php //{{ route('account.show',base64_encode($account->id)) }}
    // {{ route('account.destroy',$account->id) }}
   ?>         
