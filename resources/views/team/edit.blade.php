<?php 
  use App\Http\Controllers\TeamController;
use App\Models\Team;
use App\Http\Controllers\AccountController;
use App\Models\Account;

use Illuminate\Http\Request;



$data2 = Account::all();

 $data1 = Team::all();
?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Team</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Team</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
         
              
              <!-- Add New User-->
         <div class="card popcard" id="adduser-sec">
        <div class="card-body">
       

       <div class="pull-right">
                <a class="btn btn-info" href="{{ route('team.index') }}"> Back</a>
            </div>

          @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
          <form action="/team/edit" method="POST">
            @csrf  
          <input type="hidden" name="id" value="{{$team_edit->id}}"> 
  
    
            <div class="row">
                  <div class="col-12 col-md-6">
                       <div class="row">

                      <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Name</label> <input type="text" value="{{ $team_edit->name }}" name="name" placeholder="Enter Name" class="form-control"> 
                           <span class="text-danger">@error('name'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Trades_request</label>
                           <input type="text" class="form-control" name="trades_request" value="{{ $team_edit->trades_request }}" placeholder="Enter Trades_request">
                           <span class="text-danger">@error('trades_request'){{ $message }} @enderror</span>
                         </div>
                        </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Qc_request</label> <input type="text" value="{{ $team_edit->qc_request }}" name="qc_request" placeholder="Enter Qc_request" class="form-control"> 
                           <span class="text-danger">@error('qc_request'){{ $message }} @enderror</span>
                         </div>
                        </div>

                         <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Add_deal_potential</label> <input type="text" value="{{ $team_edit->add_deal_potential }}" name="add_deal_potential" placeholder="Enter Add_deal_potential" class="form-control"> 
                           <span class="text-danger">@error('add_deal_potential'){{ $message }} @enderror</span>
                         </div>
                        </div>
                         <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Random_did</label> <input type="text" value="{{ $team_edit->random_did }}" name="random_did" placeholder="Enter Random_did" class="form-control"> 
                           <span class="text-danger">@error('random_did'){{ $message }} @enderror</span>
                         </div>
                        </div> 

                   

                      

                    <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Account</label> 
                       <select name="account_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data2 as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('account_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Status</label>
                          <input type="hidden" name="status" value="0">
                              <input type="checkbox" name="status" value="1">
                         </div>
                        </div>


                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Edit</button>
           </form>
        </div>
        </div>
              <!-- End Of New User-->
              
           
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

@endsection

	
							
						
