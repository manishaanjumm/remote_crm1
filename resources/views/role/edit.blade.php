<?php 
 use App\Http\Controllers\RoleController;
use App\Models\ROle;
use Illuminate\Http\Request;
use App\Models\Admin;

$data_admin = Admin::where('id','=',session('LoggedUser'))->first();
// echo "<pre>";print_r($data['email']);die;



 $data1 = Role::all();
// echo "<pre>";print_r($data);die;
// echo $account_edit->id;

// die;
?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Role</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Role</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
         
              
              <!-- Add New User-->
         <div class="card popcard" id="adduser-sec">
        <div class="card-body">
        <!--  <button class="btn btn-info btn-sm pull-right" data-popcard-close="true" data-popcard-btn="adduser-btn"><a href="{{ route('account.index') }}" class="">Back</a></button>  -->
       <!--  <h4 class="card-title">Back</h4> -->

       <div class="pull-right">
                <a class="btn btn-info" href="{{ route('role.index') }}"> Back</a>
            </div>

          @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
          <form action="/role/edit" method="POST">
            @csrf  
          <input type="hidden" name="id" value="{{$role_edit->id}}"> 
  <?php //echo $role_edit->id;die;?>
    
            <div class="row">
                  <div class="col-12 col-md-6">
                       <div class="row">

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Name</label> <input type="text" value="{{$role_edit->role_name}}" name="role_name" placeholder="Enter Name" class="form-control"> 
                           <span class="text-danger">@error('role_name'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Description</label>
                         <!--   <input type="text" value="{{$role_edit->role_description}}" name="role_description" placeholder="Enter Description" class="form-control">  -->

                             <textarea type="text" class="form-control" name="role_description" 
                             value="{{$role_edit->role_description}}">{{$role_edit->role_description}}</textarea>
                           <span class="text-danger">@error('role_description'){{ $message }} @enderror</span>
                         </div>
                        </div>


                    <div class="col-12 col-md-6">
                         <div class="form-group">
                      <!--     <label for="">Admin</label> --> 
                      <input type="hidden" value="{{$data_admin['email']}}" name="admin_id"  class="form-control"  readonly /> 
                         
                         </div>
                        </div>
                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Edit</button>
           </form>
        </div>
        </div>
              <!-- End Of New User-->
              
           
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

@endsection

	
							
						
