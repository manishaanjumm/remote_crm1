<?php 
 use App\Http\Controllers\SubscriptionController;
  use App\Models\Subscription;
  use Illuminate\Support\Facades\DB;

 $data = Subscription::all();

//echo "<pre>";print_r($data);die;

?>
@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Subscription</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Subscription</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
              
              <!-- Filter Sec-->
             <!--  <div class="card popcard" id="filter-sec" style="display: none;">
                 <div class="card-body">
                  <button class="btn btn-info btn-sm pull-right" data-popcard-close="true" data-popcard-btn="filter-btn">Hide</button> 
                  <h4 class="card-title">Filter</h4>
                  <div class="row">
                   <div class="col-6 col-md-3">
                    <div class="form-group"><label for="">First Name</label> <input type="text" name="first_name" class="form-control"></div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group"><label for="">Last Name</label> <input type="text" name="last_name" class="form-control"></div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group"><label for="">Email</label> <input type="email" name="email" class="form-control"></div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group">
                       <label for="">Role</label> 
                       <select class="form-select custom-select col-12">
                        <option value="">Select One</option>
                        <option value="2">
                         staff
                        </option>
                       </select>
                    </div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group">
                       <label for="">Status</label> 
                       <select class="form-select custom-select col-12">
                        <option value="">Select One</option>
                        <option value="activated">Activated</option>
                        <option value="pending_activation">Pending Activation</option>
                        <option value="pending_approval">Pending Approval</option>
                        <option value="banned">Banned</option>
                        <option value="disapproved">Disapproved</option>
                       </select>
                    </div>
                   </div>
                   <div class="col-6 col-md-6">
                    <div class="form-group">
                       <div class="row">
                        <div class="col-12">
                         <div class="form-group">
                          <label for="">Created at</label> 
                          <div class="input-group">
                            <input type="text" name="start_date" Placeholder="Start Date" class="datepicker form-control">
                            <input type="text" name="end_date" Placeholder="End Date" class="datepicker form-control">
                          </div>
                         </div>
                        </div>
                       </div>
                    </div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group">
                       <label for="">Sort By</label> 
                       <select name="order" class="form-select">
                        <option value="first_name">First Name</option>
                        <option value="last_name">Last Name</option>
                        <option value="email">Email</option>
                        <option value="status">Status</option>
                        <option value="created_at">Created at</option>
                       </select>
                    </div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group">
                       <label for="">Order</label> 
                       <select name="order" class="form-select">
                        <option value="asc">Ascending</option>
                        <option value="desc">Descending</option>
                       </select>
                    </div>
                   </div>
                  </div>
                 </div>
              </div> -->
              <!-- End Of Filter Sec-->
              
              <!-- Add New User-->
         <div class="card popcard" id="adduser-sec">
        <div class="card-body">
        <!--  <button class="btn btn-info btn-sm pull-right" data-popcard-close="true" data-popcard-btn="adduser-btn">
          <a href="{{ route('subscription.index') }}">Hide</a></button>  -->

          <div class="pull-right">
                <a class="btn btn-info" href="{{ route('subscription.index') }}"> Back</a>
            </div>
        <h4 class="card-title">Add New Subscription</h4>

          @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
          <form action="{{ route('subscription.store') }}" method="POST">
    @csrf
            <div class="row">
                  <div class="col-12 col-md-6">
                      <div class="row">

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Type</label> <input type="text" value="{{ old('type') }}" name="type" placeholder="Enter Type" class="form-control"> 
                           <span class="text-danger">@error('type'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Amount</label> <input type="text" value="{{ old('amount') }}" name="amount" placeholder="Enter Amount" class="form-control"> 
                           <span class="text-danger">@error('amount'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-12">
                         <div class="form-group">
                          <label for="">Duration</label> <input type="text" value="{{ old('duration') }}" name="duration" placeholder="Enter Duration" class="form-control">
                           <span class="text-danger">@error('duration'){{ $message }} @enderror</span> 
                         </div>
                        </div>
                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>
           </form>
        </div>
        </div>
              <!-- End Of New User-->
              
              <!-- Table Sec-->
              <div class="card">
                 <div class="card-body">
                 <!--  <button class="btn btn-info btn-sm pull-right" data-popcard="true" data-popcard-btn-name="adduser-btn" data-popcard-name="adduser-sec"><i class="fa fa-user-plus"></i><a href="{{ route('subscription.create') }}">Create Subscription</a></button>  -->
                 <!--  <button class="btn btn-info btn-sm pull-right me-1" data-popcard="true" data-popcard-btn-name="filter-btn" data-popcard-name="filter-sec"><i class="fa fa-filter"></i> Filter</button>  -->
                  <h4 class="card-title">Subscription List</h4>
                  <h6 class="card-subtitle">Total 10 result found.</h6><br/><br/>
                  <div class="table-responsive">
                   <table class="table dataTable">
                    <thead>
                       <tr>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Duration</th>
                        <th class="table-option">Action</th>
                       </tr>
                    </thead>
                     @foreach ($data as $key => $subscription)
                  
                       <tr>
                       <td>{{ $subscription->type }}</td>
                        <td>{{ $subscription->amount }}</td>
                         <td>{{ $subscription->duration }}</td>
                        <td class="table-option">
                         <div class="btn-group">
                        <form action="{{ route('subscription.destroy',$subscription->id) }}" method="POST">
               
                          <a href="{{ route('subscription.show',$subscription->id) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View Subscription" ><i class="fa fa-arrow-circle-right"></i></a> 

 <a href="{{route('subscription.edit',$subscription->id) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View Subscription" ><i class="fa fa-edit"></i></a> 

                          <button  type="submit" class="btn btn-danger btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="Delete Subscription"><i class="fa fa-trash"></i></button>
                        </div>

   @csrf  
                  @method('DELETE')  
                  
</form>
                        </td>
                       </tr>
                        @endforeach
                  
                   </table>
                   
                  </div>
                  
                 </div>
              </div>
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

@endsection

	
							
						
