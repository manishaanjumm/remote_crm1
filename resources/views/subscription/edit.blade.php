@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Accounts</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Accounts</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
         
              
              <!-- Add New User-->
         <div class="card popcard" id="adduser-sec">
        <div class="card-body">
         <!-- <button class="btn btn-info btn-sm pull-right" data-popcard-close="true" data-popcard-btn="adduser-btn">Back</button> --> 
          <div class="pull-right">
                <a class="btn btn-info" href="{{ route('subscription.index') }}"> Back</a>
            </div>

       <!--  <h4 class="card-title">Back</h4> -->

          @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
          <form action="/subscription/edit" method="POST">
            @csrf  
          <input type="hidden" name="id" value="{{$subscription_edit->id}}"> 
   
    
            <div class="row">
                  <div class="col-12 col-md-6">
                      <div class="row">

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Type</label> <input type="text" value="{{$subscription_edit->type}}" name="type" placeholder="Enter Type" class="form-control"> 
                           <span class="text-danger">@error('type'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Amount</label> <input type="text" value="{{$subscription_edit->amount}}" name="amount" placeholder="Enter Amount" class="form-control"> 
                           <span class="text-danger">@error('amount'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-12">
                         <div class="form-group">
                          <label for="">Duration</label> <input type="text" value="{{$subscription_edit->duration}}" name="duration" placeholder="Enter Duration" class="form-control">
                           <span class="text-danger">@error('duration'){{ $message }} @enderror</span> 
                         </div>
                        </div>
                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Edit</button>
           </form>
        </div>
        </div>
              <!-- End Of New User-->
              
           
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

@endsection

	
							
						
