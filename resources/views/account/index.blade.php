<?php //echo "<pre>";print_r($accounts);die;?>


@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Account</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Account</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">

                <!-- Filter Sec-->
             <!--  <div class="card popcard" id="filter-sec" style="display: none;">
                 <div class="card-body">
                  <button class="btn btn-info btn-sm pull-right" data-popcard-close="true" data-popcard-btn="filter-btn">Hide</button> 
                  <h4 class="card-title">Filter</h4>
                  <div class="row">
                   <div class="col-6 col-md-3">
                    <div class="form-group"><label for="">Type</label>  <input type="text" value="{{ old('type') }}" name="type" placeholder="Enter Type" class="form-control"> 
                           <span class="text-danger">@error('type'){{ $message }} @enderror</span></div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group"><label for="">Account</label> <input type="text" value="{{ old('amount') }}" name="amount" placeholder="Enter Amount" class="form-control"> 
                           <span class="text-danger">@error('amount'){{ $message }} @enderror</span></div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group"><label for="">Duration</label><input type="text" value="{{ old('duration') }}" name="duration" placeholder="Enter Duration" class="form-control">
                           <span class="text-danger">@error('duration'){{ $message }} @enderror</span> </div>
                   </div>
                  
                                
                  
                  </div>
                 </div>
              </div>
 -->              <!-- End Of Filter Sec-->

  @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
                 <!-- Table Sec-->
              <div class="card">
                 <div class="card-body">
                <!--   <button class="btn btn-info btn-sm pull-right" data-popcard="true" data-popcard-btn-name="adduser-btn" data-popcard-name="adduser-sec"><i class="fa fa-user-plus"></i><a href="{{ route('account.create') }}">Create Account</a></button> --> <!-- <button class="btn btn-info btn-sm pull-right me-1" data-popcard="true" data-popcard-btn-name="filter-btn" data-popcard-name="filter-sec"><i class="fa fa-filter"></i> Filter</button>  -->
                  <div class="pull-right">
                <a class="btn btn-info" href="{{ route('account.create') }}"> Create Account</a>
            </div>
                  <h4 class="card-title">Account List</h4>
                  <h6 class="card-subtitle">Total 10 result found.</h6><br/><br/>
                  <div class="table-responsive">
                   <table class="table dataTable">
                    <thead>
                       <tr>
                        <th>Name</th>
                        <th>Subdomain</th>
                        <th>Expires At</th>
                        <th class="table-option">Action</th>
                       </tr>
                    </thead>
                     @foreach ($accounts as $key => $account)
                  
                       <tr>
                       <td>{{ $account->name }}</td>
                        <td>{{ $account->subdomain }}</td>
                         <td>{{ $account->expires_at }}</td>
                        <td class="table-option">
                         <div class="btn-group">
                        <form action="{{ route('account.destroy',$account->id) }}" method="POST">
               
                          <a href="{{ route('account.show',base64_encode($account->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View Account" ><i class="fa fa-arrow-circle-right"></i></a> 

   <a href=" {{route('account.edit',base64_encode($account->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View Account" ><i class="fa fa-edit"></i></a> 

                          <button  type="submit" class="btn btn-danger btn-sm deleteUser" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="Delete Account"><i class="fa fa-trash"></i></button>
                        </div>

   @csrf  
                  @method('DELETE')  
                  
</form>
                        </td>
                       </tr>
                        @endforeach
                  
                   </table>
                   
                  </div>
                  
                 </div>
              </div>
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).on('click','.deleteUser',function() {
        var url = $(this).attr('rel');
        if(confirm("Are you sure you want to delete this?")){
           window.location.href = url
        }
        else{
            return false;
        }
    })
</script>

@endsection

<?php //{route('subscription.edit',$subscription->id) }}?>
  