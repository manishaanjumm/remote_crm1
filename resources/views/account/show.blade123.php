@extends('layouts.template')
@section('content')

 <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Account</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Account</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">

  
<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Account</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-info" href="{{ route('account.index') }}"> Back</a>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $show_data->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Subdomain:</strong>
                {{ $show_data->subdomain }}
            </div>
        </div>
         <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Expires At:</strong>
                {{ $show_data->expires_at }}
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>

@endsection
   