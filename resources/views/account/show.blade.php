@extends('layouts.template')
@section('content')
	<div class="page-wrapper">
		<div class="container-fluid">
			<div>
					  <div class="row page-titles">
						 <div class="col-md-6 col-8 align-self-center">
							<h3 class="text-themecolor m-b-0 m-t-0">Account</h3>
							<ol class="breadcrumb">
							   <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
							   <li class="breadcrumb-item active">Account</li>
							</ol>
						 </div>
					  </div>

					  	<div class="row">
        <div class="col-lg-12 margin-tb">
            
            <div class="pull-right" style="margin-bottom: 4px">
                <a class="btn btn-info" href="{{ route('account.index') }}"> Back</a>
            </div>
        </div>
    </div>	

					  <div class="row">
						 <div class="col-12">
				 	
							<div class="card">
								<div class="card-body" style="font-size: 18px;">
									<p><small class="text-muted">Name</small> <br/> <b> {{ $show_data->name }}</b></p>
									<p><small class="text-muted">Subdomain</small> <br/> <b> {{ $show_data->subdomain }}</b> </p>
									<p><small class="text-muted">Expires At</small> <br/> <b>  {{ $show_data->expires_at }}</b> </p>
								</div>
							</div>
						 </div>
					  </div>
			</div>
		</div>
	</div>
@endsection