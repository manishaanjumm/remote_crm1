<?php 
 use App\Http\Controllers\SubscriptionController;
use App\Models\Subscription;
 //  use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AccountController;
use App\Models\Account;
use Illuminate\Http\Request;
use App\Models\Admin;

$data_admin = Admin::where('id','=',session('LoggedUser'))->first();
// echo "<pre>";print_r($data['email']);die;


 $data = Subscription::all();

 $data1 = Account::all();
// echo "<pre>";print_r($data);die;

?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Accounts</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Accounts</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
              <!-- Filter Sec-->
          <!--     <div class="card popcard" id="filter-sec" style="display: none;">
                 <div class="card-body">
                  <button class="btn btn-info btn-sm pull-right" data-popcard-close="true" data-popcard-btn="filter-btn">Hide</button> 
                  <h4 class="card-title">Filter</h4>
                  <div class="row">
                   <div class="col-6 col-md-3">
                    <div class="form-group"><label for="">First Name</label> <input type="text" name="first_name" class="form-control"></div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group"><label for="">Last Name</label> <input type="text" name="last_name" class="form-control"></div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group"><label for="">Email</label> <input type="email" name="email" class="form-control"></div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group">
                       <label for="">Role</label> 
                       <select class="form-select custom-select col-12">
                        <option value="">Select One</option>
                        <option value="2">
                         staff
                        </option>
                       </select>
                    </div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group">
                       <label for="">Status</label> 
                       <select class="form-select custom-select col-12">
                        <option value="">Select One</option>
                        <option value="activated">Activated</option>
                        <option value="pending_activation">Pending Activation</option>
                        <option value="pending_approval">Pending Approval</option>
                        <option value="banned">Banned</option>
                        <option value="disapproved">Disapproved</option>
                       </select>
                    </div>
                   </div>
                   <div class="col-6 col-md-6">
                    <div class="form-group">
                       <div class="row">
                        <div class="col-12">
                         <div class="form-group">
                          <label for="">Created at</label> 
                          <div class="input-group">
                            <input type="text" name="start_date" Placeholder="Start Date" class="datepicker form-control">
                            <input type="text" name="end_date" Placeholder="End Date" class="datepicker form-control">
                          </div>
                         </div>
                        </div>
                       </div>
                    </div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group">
                       <label for="">Sort By</label> 
                       <select name="order" class="form-select">
                        <option value="first_name">First Name</option>
                        <option value="last_name">Last Name</option>
                        <option value="email">Email</option>
                        <option value="status">Status</option>
                        <option value="created_at">Created at</option>
                       </select>
                    </div>
                   </div>
                   <div class="col-6 col-md-3">
                    <div class="form-group">
                       <label for="">Order</label> 
                       <select name="order" class="form-select">
                        <option value="asc">Ascending</option>
                        <option value="desc">Descending</option>
                       </select>
                    </div>
                   </div>
                  </div>
                 </div>
              </div> -->
              <!-- End Of Filter Sec-->
              
              <!-- Add New User-->
              <div class="card popcard" id="adduser-sec">
                 <div class="card-body">
                 <!--  <button class="btn btn-info btn-sm pull-right" data-popcard-close="true" data-popcard-btn="adduser-btn">
                    <a href="{{ route('account.index') }}">Hide</a></button>  -->

                     <div class="pull-right">
                <a class="btn btn-info" href="{{ route('account.index') }}"> Back</a>
            </div>
                  <h4 class="card-title">Add New Account</h4>

                     @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif

                  <form action="{{ route('account.save') }}" method="POST">
    @csrf
                   
                   <div class="row">
                  <div class="col-12 col-md-6">
                      <div class="row">

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Name</label> <input type="text" value="{{ old('name') }}" name="name" placeholder="Enter Name" class="form-control"> 
                           <span class="text-danger">@error('name'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Subdomain</label> <input type="text" value="{{ old('subdomain') }}" name="subdomain" placeholder="Enter Subdomain" class="form-control"> 
                           <span class="text-danger">@error('subdomain'){{ $message }} @enderror</span>
                         </div>
                        </div>

                     <div class="col-12 col-md-6">
                    <div class="form-group">
                          <label for="">Expires_at</label> 
                            <input type="date" name="expires_at" Placeholder="" class="form-control">
                             <span class="text-danger">@error('expires_at'){{ $message }} @enderror</span>
                          </div>
                         </div>
                        

                    <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Subscription Type</label> 
                       <select name="subscription_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->type }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('subscription_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Admin</label> <input type="text" value="{{$data_admin['email']}}" name="admins_id"  class="form-control"  readonly /> 
                         
                         </div>
                        </div>
                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>
                  
                  </form>
                 </div>
              </div>
              <!-- End Of New User-->
              
           
             <!-- Table Sec-->
              <div class="card">
                 <div class="card-body">
                 <!--  <button class="btn btn-info btn-sm pull-right" data-popcard="true" data-popcard-btn-name="adduser-btn" data-popcard-name="adduser-sec"><i class="fa fa-user-plus"></i><a href="{{ route('account.create') }}">Create Account</a></button>  -->
                <!--   <button class="btn btn-info btn-sm pull-right me-1" data-popcard="true" data-popcard-btn-name="filter-btn" data-popcard-name="filter-sec"><i class="fa fa-filter"></i> Filter</button>  -->
                  <h4 class="card-title">Account List</h4>
                  <h6 class="card-subtitle">Total 10 result found.</h6><br/><br/>
                  <div class="table-responsive">
                   <table class="table dataTable">
                    <thead>
                       <tr>
                        <th>Name</th>
                        <th>Subdomain</th>
                        <th>Expires At</th>
                        <th class="table-option">Action</th>
                       </tr>
                    </thead>
                     @foreach ($data1 as $key => $account)
                  
                       <tr>
                       <td>{{ $account->name }}</td>
                        <td>{{ $account->subdomain }}</td>
                         <td>{{ $account->expires_at }}</td>
                        <td class="table-option">
                         <div class="btn-group">
                        <form action="{{ route('account.destroy',$account->id) }}" method="POST">
               
                          <a href="{{ route('account.show',base64_encode($account->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View Account" ><i class="fa fa-arrow-circle-right"></i></a> 

   <a href=" {{route('account.edit',base64_encode($account->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View Account" ><i class="fa fa-edit"></i></a> 

                          <button  type="submit" class="btn btn-danger btn-sm deleteUser" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="Delete Account"><i class="fa fa-trash"></i></button>
                        </div>

   @csrf  
                  @method('DELETE')  
                  
</form>
                        </td>
                       </tr>
                        @endforeach
                  
                   </table>
                   
                  </div>
                  
                 </div>
              </div>
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->
       <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).on('click','.deleteUser',function() {
        var url = $(this).attr('rel');
        if(confirm("Are you sure you want to delete this?")){
           window.location.href = url
        }
        else{
            return false;
        }
    })
</script>

@endsection

  
              
            
