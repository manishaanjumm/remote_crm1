<?php 
 use App\Http\Controllers\SubscriptionController;
use App\Models\Subscription;
 //  use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AccountController;
use App\Models\Account;
use Illuminate\Http\Request;
use App\Models\Admin;

$data_admin = Admin::where('id','=',session('LoggedUser'))->first();
// echo "<pre>";print_r($data['email']);die;


 $data = Subscription::all();

 $data1 = Account::all();
// echo "<pre>";print_r($data);die;
// echo $account_edit->id;

// die;
?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Accounts</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Accounts</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
         
              
              <!-- Add New User-->
         <div class="card popcard" id="adduser-sec">
        <div class="card-body">
        <!--  <button class="btn btn-info btn-sm pull-right" data-popcard-close="true" data-popcard-btn="adduser-btn"><a href="{{ route('account.index') }}" class="">Back</a></button>  -->
       <!--  <h4 class="card-title">Back</h4> -->

       <div class="pull-right">
                <a class="btn btn-info" href="{{ route('account.index') }}"> Back</a>
            </div>

          @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
          <form action="/account/edit" method="POST">
            @csrf  
          <input type="hidden" name="id" value="{{$account_edit->id}}"> 
  <?php //echo $account_edit->id_d;die;?>
    
            <div class="row">
                  <div class="col-12 col-md-6">
                       <div class="row">

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Name</label> <input type="text" value="{{$account_edit->name}}" name="name" placeholder="Enter Name" class="form-control"> 
                           <span class="text-danger">@error('type'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Subdomain</label> <input type="text" value="{{$account_edit->subdomain}}" name="subdomain" placeholder="Enter Subdomain" class="form-control"> 
                           <span class="text-danger">@error('amount'){{ $message }} @enderror</span>
                         </div>
                        </div>

                     <div class="col-12 col-md-6">
                    <div class="form-group">
                          <label for="">Expires_at</label> 
                            <input type="date" name="expires_at" Placeholder="" class="form-control">
                             <span class="text-danger">@error('expires_at'){{ $message }} @enderror</span>
                          </div>
                         </div>
                        

                    <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Subscription Type</label> 
                       <select name="subscription_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->type }}</option>
                    @endforeach
                       </select>
                         <span class="text-danger">@error('subscription_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Admin</label> <input type="text" value="{{$data_admin['email']}}" name="admins_id"  class="form-control"  readonly /> 
                         
                         </div>
                        </div>
                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Edit</button>
           </form>
        </div>
        </div>
              <!-- End Of New User-->
              
           
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

@endsection

	
							
						
