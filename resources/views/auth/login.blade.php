
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>
    <meta name="description" content="">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
</head>
<body>

	<div class="main-sec">
		<!--Content Sec-->
				<div class="login-register loginbg" >
				    <center><a href="#" style="display: block;margin:20px 0;"><b><img src="images/default_sidebar_logo.png" alt=""  style="max-height: 43px;"> </b> <span ><img src="images/default_main_logo.png" alt=""  style="max-height: 37px;"> </span></a></center>
					<div class="login-box card">
						<div class="card-body">
							<form id="loginform" class="form-horizontal form-material" method="post" 
							action="{{ route('auth.check') }}">

							  @if (Session::get('fail'))
                        <div class="alert alert-danger" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
							@csrf
								<h3 class="box-title m-b-20">Login</h3>
								<div class="form-group">
									<input type="text" name="email" placeholder="Email" class="form-control" 
									value="{{ old('email') }}"/>
									<!---->
									 <span class="text-danger">@error('email'){{ $message }} @enderror</span>
								</div>
								<div class="form-group">
									<input type="password" name="password" placeholder="Password" class="form-control" />
									 <span class="text-danger">@error('password'){{ $message }} @enderror</span>
									<!---->
								</div>
								<div class="form-group text-center m-t-20"><button type="submit" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light">Sign In</button></div>
								<div class="form-group m-b-0">
									<div class="col-sm-12 text-center">
										<p>
											Forgot your Password? <a href="/password" class="text-info m-l-5"><b>Reset here!</b></a>
										</p>
									</div>
								</div>
								<div class="row m-t-10">
									<div class="col-12 text-center"><button type="button" class="btn bt-block btn-info">Login as Admin</button></div>
								</div>
							</form>
						</div>
						
					</div>
				</div>
		<!--End of Content Sec-->
	</div>	
</body>
</html>
		