<?php 
use Illuminate\Http\Request;
use App\Models\Admin;

$data = Admin::where('id','=',session('LoggedUser'))->first();
// echo "<pre>";print_r($data['email']);die;
?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CRM</title>
    <!-- @yield('title') -->
    <base href="{{\URL::to('/')}}">
    <meta name="description" content="">
     <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
   <!--  <link rel="shortcut icon" href="images/favicon.ico"> -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}" />
	<!-- <link rel="stylesheet" href="css/font-awesome.css"> -->
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/style.css') }}" />
   <!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}" />
   <!--  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"> -->
   <link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables.bootstrap5.min.css') }}" />
  <!--   <link href="css/dataTables.bootstrap5.min.css" rel="stylesheet" type="text/css"> -->
   <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}" />
 <!--  <link rel="stylesheet" href="css/jquery-ui.css"> -->

</head>
<body>

	<div class="main-sec">
		<!--HEADER SEC-->
		<header class="topbar fixed-top " style="">
			<nav class="navbar top-navbar navbar-expand-md navbar-light">
				<div class="navbar-header">
					<a href="#" class="navbar-brand active"><b><img src="images/default_sidebar_logo.png" alt=""  style="max-height: 43px;"> </b> <span class="d-none d-lg-inline"><img src="images/default_main_logo.png" alt=""  style="max-height: 37px;"> </span></a>
				</div>
				<div class="navbar-collapse">
					<ul class="navbar-nav mr-auto mt-md-0 ">
						<li class="nav-item" ><a href="#" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Toggle Sidebar" class="nav-link toggle-sidebar"><i class="icon-arrow-left-circle fa fa-bars"></i></a></li>
					</ul>
					<ul class="navbar-nav my-lg-0">
						<li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="bottom" title="To Do"><a href="#" class="nav-link"><i class="fa fa-check-circle"></i></a></li>
						<li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Message"><a href="#" class="nav-link"><i class="fa fa-envelope"></i></a></li>
						<li class="nav-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Configuration">
							<a href="#" class="nav-link dropdown-toggle"  id="configDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-cogs"></i></a>
							<div class="dropdown-menu dropdown-menu-right" aria-labelledby="configDropdown">
							  <ul class="dropdown-user">
								 <li><a href="#" class=""><i class="fa fa-cogs"></i> System Configuration</a></li>
								 <li><a href="#" class=""><i class="fa fa-server fa-fw"></i> Server Configuration</a></li>
								 <li><a href="#" class=""><i class="fa fa-desktop fa-fw"></i> Frontend Configuration</a></li>
								 <li><a href="#" class=""><i class="fa fa-dollar fa-fw"></i> Payment Configuration</a></li>
								 <li><a href="#" class=""><i class="fa fa-database"></i> Backup</a></li>
								 <li><a href="#" class=""><i class="fa fa-envelope"></i> Email Template</a></li>
								 <li><a href="#" class=""><i class="fa fa-folder"></i> Email Log</a></li>
								 <li><a href="#" class=""><i class="fa fa-bars"></i> Activity Log</a></li>
							  </ul>
						   </div>
						</li>
						<li class="nav-item dropdown">
						   <a href="#" class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"><img src="images/avatar.png" alt="user" class="profile-pic"></a> 
						   <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
							  <ul class="dropdown-user">
								 <li>
									<div class="dw-user-box text-center">
									   <div class="u-img"><img src="images/avatar.png" alt="user"></div>
									   <div class="u-text">
										  <h4>{{$data['name']}}</h4>
										  <p class="text-muted">{{$data['email']}}</p>
										  <a href="#" class="btn btn-rounded btn-danger btn-sm">View Profile</a>
									   </div>
									</div>
								 </li>
								 <li role="separator" class="divider"></li>
								 <li><a href="#" class=""><i class="fa fa-cogs"></i> Change Password</a></li>
								 <li><a href="#" class=""><i class="fa fa-user"></i> About</a></li>
								 <li><a href="#" class=""><i class="fa fa-life-ring"></i> Support</a></li>
								 <li><a href="#" class=""><i class="fa fa-download"></i> Update</a></li>
								 <li role="separator" class="divider"></li>
								 <li><a href="{{ route('auth.logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
							  </ul>
						   </div>
						</li>
						
						</ul>
				</div>
			</nav>
		</header>
				
			
		<!--End Of Header Sec-->
		<!--Sidebar-->
		<div class="left-sidebar">
		<nav class="sidebar-nav m-t-20">
			<ul id="sidebarnav">
			   <li><a href="{{ route('admin.dashboard') }}" class=""><i class="fa fa-home fa-fw"></i> <span class="hide-menu">Home</span></a></li>
			   <li><a href="{{ route('account.index') }}" class=""><i class="fa fa-users fa-fw"></i> <span class="hide-menu">Account</span></a></li>
			   <li><a href="{{ route('role.index') }}" class=""><i class="fa fa-dollar fa-fw"></i> <span class="hide-menu">Role</span></a></li>
			   <li><a href="{{ route('permission.index') }}" class=""><i class="fa fa-file fa-fw"></i> <span class="hide-menu">Permission</span></a></li>
			   
			   <!--  <li><a href="{ route('roletopermission.index') }}" class=""><i class="fa fa-briefcase fa-fw"></i> <span class="hide-menu">RoleToPermission</span></a></li> -->
			    <li><a href="{{ route('product.index') }}" class=""><i class="fa fa-bullhorn fa-fw"></i> <span class="hide-menu">Product</span></a></li>
			    <li><a href="{{ route('team.index') }}" class=""><i class="fa fa-briefcase fa-fw"></i> <span class="hide-menu">Team</span></a></li>
			     <li><a href="{{ route('staff.index') }}" class=""><i class="fa fa-life-ring fa-fw"></i> <span class="hide-menu">Staff</span></a></li>
			 <!--   <li><a href="{ route('subscription.index') }}" class=""><i class="fa fa-life-ring fa-fw"></i> <span class="hide-menu">Subscription</span></a></li> -->
			  <!--  <li><a href="#" class=""><i class="fa fa-question-circle fa-fw"></i> <span class="hide-menu">Query</span></a></li> -->
			</ul>
		</nav>
		</div>
		<!--End Of Sidebar-->
	
 
@yield('content')
@include('includes.footer')
