<?php 
 use App\Http\Controllers\PermissionController;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\RoleController;
use App\Models\Role;


 $data1 = Permission::all();
 $data4 = Role::all();
// echo "<pre>";print_r($data);die;

?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Permission</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Permission</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
           
              
              <!-- Add New User-->
              <div class="card popcard" id="adduser-sec">
                 <div class="card-body">
                 <!--  <button class="btn btn-info btn-sm pull-right" data-popcard-close="true" data-popcard-btn="adduser-btn">
                    <a href="{{ route('account.index') }}">Hide</a></button>  -->

                     <div class="pull-right">
                <a class="btn btn-info" href="{{ route('permission.index') }}"> Back</a>
            </div>
                  <h4 class="card-title">Add New Permission</h4>

                     @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif

                  <form action="{{ route('permission.save') }}" method="POST">
    @csrf
               
                   <div class="row">
                  <div class="col-12 col-md-6">
                      <div class="row">
 
                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Name</label> <input type="text" value="{{ old('permission_name') }}" name="permission_name" placeholder="Enter Name" class="form-control"> 
                           <span class="text-danger">@error('permission_name'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Description</label> 
                           <textarea type="text" class="form-control" name="permission_description" value="{{ old('permission_description') }}" placeholder="Enter Description"></textarea>
    
                           <span class="text-danger">@error('permission_description'){{ $message }} @enderror</span>
                         </div>
                        </div>

                            <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Role</label> 
                       <select name="role_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data4 as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->role_name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('role_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                         <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Allowed</label> <input type="text" value="{{ old('allowed') }}" name="allowed" placeholder="Enter..." class="form-control"> 
                           <span class="text-danger">@error('allowed'){{ $message }} @enderror</span>
                         </div>
                        </div>


                          <!--  <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Status</label>
                          <input type="hidden" name="status" value="0">
                              <input type="checkbox" name="status" value="1">
                         </div>
                        </div> -->
                   
                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Add</button>
                  
                  </form>
                 </div>
              </div>
              <!-- End Of New User-->
              
           
             <!-- Table Sec-->
              <div class="card">
                 <div class="card-body">
                
                  <h4 class="card-title">Permission List</h4>
                  <h6 class="card-subtitle">Total 10 result found.</h6><br/><br/>
                  <div class="table-responsive">
                   <table class="table dataTable">
                    <thead>
                       <tr>
                        <th>Name</th>
                        <th>Description</th>
                         <th>Allowed</th>
                      
                        <th class="table-option">Action</th>
                       </tr>
                    </thead>
                     @foreach ($data1 as $key => $permission)
                  
                       <tr>
                       <td>{{ $permission->permission_name }}</td>
                        <td>{{ $permission->permission_description }}</td>
                          <td>{{ $permission->allowed }}</td>
                         
                        <td class="table-option">
                         <div class="btn-group">
                        <form action="{{ route('permission.destroy',$permission->id) }}" method="POST">
            
                          <a href="{{ route('permission.show',base64_encode($permission->id)) }} " class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View Permission" ><i class="fa fa-arrow-circle-right"></i></a> 

   <a href="{{route('permission.edit',base64_encode($permission->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View Permission" ><i class="fa fa-edit"></i></a> 

                          <button  type="submit" class="btn btn-danger btn-sm deleteUser" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="Delete Permission"><i class="fa fa-trash"></i></button>
                        </div>

   @csrf  
                  @method('DELETE')  
                  
</form>
                        </td>
                       </tr>
                        @endforeach
                  
                   </table>
                   
                  </div>
                  
                 </div>
              </div>
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->
       <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).on('click','.deleteUser',function() {
        var url = $(this).attr('rel');
        if(confirm("Are you sure you want to delete this?")){
           window.location.href = url
        }
        else{
            return false;
        }
    })
</script>

@endsection

  
         
            
