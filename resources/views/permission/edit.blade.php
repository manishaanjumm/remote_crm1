<?php 
 use App\Http\Controllers\PermissionController;
use App\Models\Permission;
use Illuminate\Http\Request;


 $data1 = Permission::all();
// echo "<pre>";print_r($data);die;
// echo $account_edit->id;

// die;
?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Permission</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Permission</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
         
              
              <!-- Add New User-->
         <div class="card popcard" id="adduser-sec">
        <div class="card-body">
        <!--  <button class="btn btn-info btn-sm pull-right" data-popcard-close="true" data-popcard-btn="adduser-btn"><a href="{{ route('account.index') }}" class="">Back</a></button>  -->
       <!--  <h4 class="card-title">Back</h4> -->

       <div class="pull-right">
                <a class="btn btn-info" href="{{ route('permission.index') }}"> Back</a>
            </div>

          @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
          <form action="/permission/edit" method="POST">
            @csrf  
          <input type="hidden" name="id" value="{{$permission_edit->id}}"> 
  
    
            <div class="row">
                  <div class="col-12 col-md-6">
                       <div class="row">

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Name</label> <input type="text" value="{{$permission_edit->permission_name}}" name="permission_name" placeholder="Enter Name" class="form-control"> 
                           <span class="text-danger">@error('permission_name'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Description</label>
                       
                             <textarea type="text" class="form-control" name="permission_description" 
                             value="{{$permission_edit->permission_description}}">{{$permission_edit->permission_description}}</textarea>
                           <span class="text-danger">@error('permission_description'){{ $message }} @enderror</span>
                         </div>
                        </div>

                         <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Allowed</label> <input type="text" value="{{$permission_edit->allowed}}" name="allowed" placeholder="Enter.." class="form-control"> 
                           <span class="text-danger">@error('allowed'){{ $message }} @enderror</span>
                         </div>
                        </div>


                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Edit</button>
           </form>
        </div>
        </div>
              <!-- End Of New User-->
              
           
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

@endsection

	
							
						
