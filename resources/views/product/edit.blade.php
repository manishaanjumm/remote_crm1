<?php 
  use App\Http\Controllers\ProductController;
use App\Models\Product;
use App\Http\Controllers\AccountController;
use App\Models\Account;
use App\Http\Controllers\RoleController;
use App\Models\Role;
use Illuminate\Http\Request;


 $data = Role::all();
$data2 = Account::all();

 $data1 = Product::all();
?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Products</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Products</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
         
              
              <!-- Add New User-->
         <div class="card popcard" id="adduser-sec">
        <div class="card-body">
       

       <div class="pull-right">
                <a class="btn btn-info" href="{{ route('product.index') }}"> Back</a>
            </div>

          @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
          <form action="/product/edit" method="POST">
            @csrf  
          <input type="hidden" name="id" value="{{$product_edit->id}}"> 
  
    
            <div class="row">
                  <div class="col-12 col-md-6">
                       <div class="row">

                     

                         <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Name</label> <input type="text" value="{{$product_edit->name}}" name="name" placeholder="Enter Name" class="form-control"> 
                           <span class="text-danger">@error('name'){{ $message }} @enderror</span>
                         </div>
                        </div>

                        <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Description</label>
                           <textarea type="text" class="form-control" name="description" value="{{$product_edit->description}}" placeholder="Enter Description">{{$product_edit->description}}</textarea>
                           <span class="text-danger">@error('description'){{ $message }} @enderror</span>
                         </div>
                        </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Currency</label> <input type="text" value="{{$product_edit->currency}}" name="currency" placeholder="Enter Currency" class="form-control"> 
                           <span class="text-danger">@error('currency'){{ $message }} @enderror</span>
                         </div>
                        </div>

                         <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Value</label> <input type="text" value="{{$product_edit->value}}" name="value" placeholder="Enter Value" class="form-control"> 
                           <span class="text-danger">@error('value'){{ $message }} @enderror</span>
                         </div>
                        </div>
                        

                    <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Role</label> 
                       <select name="role_id" class="form-select">
                       <option value="{{$product_edit->value}}">--- Select ---</option>
                    @foreach ($data as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->role_name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('role_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Account</label> 
                       <select name="account_id" class="form-select">
                       <option value="">--- Select ---</option>
                    @foreach ($data2 as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('account_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                    <div class="col-12 col-md-6">
                         <div class="form-group">
                          <label for="">Status</label>
                          <input type="hidden" name="status" value="0">
                              <input type="checkbox" name="status" value="1">
                         </div>
                        </div>


                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Edit</button>
           </form>
        </div>
        </div>
              <!-- End Of New User-->
              
           
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

@endsection

	
							
						
