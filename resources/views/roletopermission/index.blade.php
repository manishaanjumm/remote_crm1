<?php use App\Http\Controllers\RoleController;
use App\Models\Role;
use App\Http\Controllers\PermissionController;
use App\Models\Permission;
?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Role To Permission</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Role To Permission</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">

            

  @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
                 <!-- Table Sec-->
              <div class="card">
                 <div class="card-body">
                
                  <div class="pull-right">
                <a class="btn btn-info" href="{{ route('roletopermission.create') }}"> Create Role To Permission</a>
            </div>
                 <h4 class="card-title">Role To Permission List</h4>
                  <h6 class="card-subtitle">Total 10 result found.</h6><br/><br/>
                  <div class="table-responsive">
                   <table class="table dataTable">
                    <thead>
                       <tr>
                        <th>Role Name</th>
                        <th>Permission Name</th>
                        <th class="table-option">Action</th>
                       </tr>
                    </thead>
                     @foreach ($roletopermissions as $key => $roletopermission)
                <?php  $check = Role::where('id','=',$roletopermission->role_id)->first();
                     //echo "<pre>";print_r($check);die;
                       $role_name = $check['role_name'];
                      $check1 = Permission::where('id','=',$roletopermission->permission_id)->first();
                     //echo "<pre>";print_r($check);die;
                    $permission_name = $check1['permission_name'];
                ?>
                       <tr>
                       <td>{{$role_name}}</td>
                        <td>{{$permission_name}}</td>
                       
                        <td class="table-option">
                         <div class="btn-group">
                        <form action="{{ route('roletopermission.destroy',$roletopermission->id) }}" method="POST">
               
                          <a href="{{ route('roletopermission.show',base64_encode($roletopermission->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View User" ><i class="fa fa-arrow-circle-right"></i></a> 

   <a href="{{ route('roletopermission.edit',base64_encode($roletopermission->id)) }}" class="btn btn-info btn-sm" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="View User" ><i class="fa fa-edit"></i></a> 

                          <button  type="submit" class="btn btn-danger btn-sm deleteUser" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-original-title="Delete User"><i class="fa fa-trash"></i></button>
                        </div>

   @csrf  
                  @method('DELETE')  
                  
</form>
                        </td>
                       </tr>
                        @endforeach
                  
                   </table>
                   
                  </div>
                  
                 </div>
              </div>
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).on('click','.deleteUser',function() {
        var url = $(this).attr('rel');
        if(confirm("Are you sure you want to delete this?")){
           window.location.href = url
        }
        else{
            return false;
        }
    })
</script>

@endsection

<?php //{route('subscription.edit',$subscription->id) }}?>
  