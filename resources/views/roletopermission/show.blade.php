<?php use App\Http\Controllers\RoleController;
use App\Models\Role;
use App\Http\Controllers\PermissionController;
use App\Models\Permission;
?>
@extends('layouts.template')
@section('content')
	<div class="page-wrapper">
		<div class="container-fluid">
			<div>
					  <div class="row page-titles">
						 <div class="col-md-6 col-8 align-self-center">
							<h3 class="text-themecolor m-b-0 m-t-0">Role To Permission</h3>
							<ol class="breadcrumb">
							   <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
							   <li class="breadcrumb-item active">Role To Permission</li>
							</ol>
						 </div>
					  </div>

					  	<div class="row">
        <div class="col-lg-12 margin-tb">
            
            <div class="pull-right" style="margin-bottom: 4px">
                <a class="btn btn-info" href="{{ route('roletopermission.index') }}"> Back</a>
            </div>
        </div>
    </div>	
 <?php  $check = Role::where('id','=',$show_data->role_id)->first();
                     //echo "<pre>";print_r($check);die;
                       $role_name = $check['role_name'];
                      $check1 = Permission::where('id','=',$show_data->permission_id)->first();
                     //echo "<pre>";print_r($check);die;
                    $permission_name = $check1['permission_name'];
                ?>
					  <div class="row">
						 <div class="col-12">
				 	
							<div class="card">
								<div class="card-body" style="font-size: 18px;">
									<p><small class="text-muted">Role</small> <br/> <b> {{ $role_name }}</b></p>
									<p><small class="text-muted">Permission</small> <br/> <b> {{ $permission_name }}</b> </p>
									
								</div>
							</div>
						 </div>
					  </div>
			</div>
		</div>
	</div>
@endsection