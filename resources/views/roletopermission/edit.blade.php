<?php 
 use App\Http\Controllers\RoleToPermissionController;
use App\Models\RoleToPermission;
 
use App\Http\Controllers\RoleController;
use App\Models\Role;
use App\Http\Controllers\PermissionController;
use App\Models\Permission;
use Illuminate\Http\Request;

 $data = Role::all();
 $data2 = Permission::all();

 $data1 = RoleToPermission::all();

?>

@extends('layouts.template')
@section('content')
  

    <!--Content Sec-->
      <div class="page-wrapper" style="min-height: 1000px;">
        <div class="container-fluid">
           <div>
            <div class="row page-titles">
             <div class="col-md-6 col-8 align-self-center">
              <h3 class="text-themecolor m-b-0 m-t-0">Role To Permission</h3>
              <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}" class="">Home</a></li>
                 <li class="breadcrumb-item active">Role To Permission</li>
              </ol>
             </div>
            </div>
            <div class="row">
             <div class="col-12">
              
         
              
              <!-- Add New User-->
         <div class="card popcard" id="adduser-sec">
        <div class="card-body">
      
       <div class="pull-right">
                <a class="btn btn-info" href="{{ route('roletopermission.index') }}"> Back</a>
            </div>

          @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                      @if (Session::get('fail'))
                        <div class="alert alert-fail" role="alert">
                            {{ Session::get('fail') }}
                        </div>
                    @endif
          <form action="/roletopermission/edit" method="POST">
            @csrf  
          <input type="hidden" name="id" value="{{$roletopermission_edit->id}}"> 
 
    
            <div class="row">
                  <div class="col-12 col-md-6">
                       <div class="row">
  <?php  $check = Role::where('id','=',$roletopermission_edit->role_id)->first();
                     //echo "<pre>";print_r($check);die;
                       $role_name = $check['role_name'];
                      
                      $check1 = Permission::where('id','=',$roletopermission_edit->permission_id)->first();
                     //echo "<pre>";print_r($check);die;
                    $permission_name = $check1['permission_name'];
                ?>

                    <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Role</label> 
                       <select name="role_id" class="form-select">
                       <option value="{{$roletopermission_edit->role_id}}"> previous---{{$role_name}}---</option>
                    @foreach ($data as $key => $value)
                    <option value="{{ $value->id }}">{{ $value->role_name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('role_id'){{ $message }} @enderror</span>
                    </div>
                   </div>


                     <div class="col-12 col-md-6">
                    <div class="form-group">
                       <label for="">Permission</label> 
                       <select name="permission_id" class="form-select">
                       <option value="{{$roletopermission_edit->permission_id}}">previous---{{$permission_name}}---</option>
                    @foreach ($data2 as $key => $value1)
                    <option value="{{ $value1->id }}">{{ $value1->permission_name }}</option>
                    @endforeach
                       </select>
                        <span class="text-danger">@error('permission_id'){{ $message }} @enderror</span>
                    </div>
                   </div>

                  
                       
                      </div>
                  </div> 
              </div>
           <button type="submit" class="btn btn-info waves-effect waves-light">Edit</button>
           </form>
        </div>
        </div>
              <!-- End Of New User-->
              
           
             </div>
            </div>
           </div>
        </div>
      </div>
       
    <!--End Of Content sec-->

@endsection

	
							
						
