$(document).ready(function(){
  $('[data-bs-toggle="tooltip"]').tooltip({ trigger: "hover" });
  $('.toggle-sidebar').click(function(){
	  $('.main-sec').toggleClass('short-sidebar');
  });
  $('.dataTable').DataTable();
  
  $('.datepicker').datepicker();
  
  $('[data-popcard="true"]').click(function(){
	  var popcardName = '#'+ $(this).attr('data-popcard-name');
	  $(popcardName).show();
	  $(this).hide();
  });
  $('[data-popcard-close="true"]').click(function(){
	  $(this).parents(".popcard").hide();
      var popbtn=$(this).attr('data-popcard-btn');
      $("[data-popcard-btn-name="+popbtn+"]").show();	  
	  
  });
	  
   if($(window).width() < 1170) {
           $(".main-sec").addClass("short-sidebar"); 
        } 
});